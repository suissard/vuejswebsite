::les commande  ne s'afficheront pas dans la console
ECHO OFF

::Indique le démarrage du processus
echo .
echo GIT PUSH demarre
echo =====================================
echo .

::Ajout de tout les fichier dans la prise en compte de git
git add .

::Creer un commit ayant pour nom la date et l'heure
git commit

::envoie du code sur gitLab
git push origin master

::Vous devez ensuite manuellement rentrer votre code

::Indique la fin du processus
echo .
echo =====================================
echo GIT PUSH termine
echo .

::Lien pour qeu la fenettre de commande ne se ferme pas
timeout /t -1 /nobreak >NUL