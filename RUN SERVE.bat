::les commande  ne s'afficheront pas dans la console
ECHO OFF

::Indique le démarrage du processus
echo .
echo Serveur démarre
echo =====================================
echo .

::Lancement du script de server via npm
npm run serve


::Indique la fin du processus
echo .
echo =====================================
echo Serveur termine
echo .

::Lien pour qeu la fenettre de commande ne se ferme pas
timeout /t -1 /nobreak >NUL