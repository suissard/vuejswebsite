//gere toute les importations
import Vue from 'vue'
Vue.config.productionTip = false

import vuetify from '@/plugins/vuetify'
import router from './router'
import App from './App.vue'

// Impmort de moduel a "Use"
import vueCookie from 'vue-cookies'
import Vuetify from 'vuetify/lib';
Vue.use(vueCookie)
Vue.$cookies.config('15d')
Vue.use(Vuetify);


new Vue({
  //components,
  vuetify,
  router,
  render: h => h(App),
}).$mount('#app')
