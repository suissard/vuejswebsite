import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

import GLOBAL from "../GLOBAL.js";



const vuetify = new Vuetify({
  theme: {
    dark: true,
    options: {
      customProperties: true,
      variations: true,
    },

    themes: GLOBAL.THEME
  },
})


export default vuetify



