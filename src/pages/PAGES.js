const PAGES = {}
const ROUTES = []
//const NAVBAR = {}

/**
 * Class dédiée aux pages du site web et détermine si elle apparaissent dans l'objet GLOBAL
 *
 */
class Page {
    constructor(component, navBar = false, url) {
        if (!component) throw new Error(`Aucun composant valide fournit`)
        if (!component.name) throw new Error(`Le composant ${component.__file} doit posséder un "name"`)

        this.id = component.name
        this.component = component
        this.active = true


        this.url = component.url
        if (!this.url) this.url = "/" + this.id.toLowerCase() //Si pas de "url", indique une url par defaut basé sur le "name"
        if (url) this.url = url //permet un url personnalisé indépendament du composant

        //OPTIONNEL
        this.icon = component.icon
        this.navBar = navBar

        //Ajout des info pertinentes dans l'objet GLOBAL
        PAGES[this.id] = this
        ROUTES.push({ path: this.url, component })

    }
}

//Importation des composants de page
import Home from './Home.vue'
import Bot from './Bot.vue'
import Recherche from './Recherche.vue'
import Organisation from './Organisation.vue'
import Soutien from './Soutien.vue'
import Equipes from './Equipes.vue'
import Classement from './Classement.vue'
import Participer from './Participer.vue'
import Blog from './Blog.vue'
import Authentification from './Authentification.vue'
import UserParameters from './UserParameters.vue'


//Creation de pages prise en compte par le router mais n'aparaissant pas dnas la navbar
new Page(Home)
new Page(Authentification)
new Page(Bot)
new Page(Recherche)
new Page(Organisation)
new Page(Soutien)
new Page(UserParameters)

//Pages apparaissant dans la navBar => Ordre d'instanciation definit l'ordre dans la navbar
new Page(Equipes, true)
new Page(Classement, true)
new Page(Participer, true)
new Page(Blog, true)

export default { PAGES, ROUTES}

/*
TODO
On garde */