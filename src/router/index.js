import Vue from 'vue'
import VueRouter from 'vue-router'
import GLOBAL from '../GLOBAL.js'

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes:GLOBAL.ROUTES
  }
)

export default router
