//simulation d'un objet Utilisateur, recuperable a terme via API

class Event {
  constructor(event) {
    this.set(event)
  }
  set(value) {
    for (let i in value) {
      this[i] = value[i]
    }
  }
}
let milliParJour = 1000 * 60 * 60 * 24;
let timestamp = Date.UTC(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

[timestamp - milliParJour * 3]

let event1 = {
  id: timestamp - milliParJour,
  start: new Date(timestamp - milliParJour),
  end: new Date(timestamp - milliParJour + 60 * 60 * 1000 * 2),
  status: "confirmed",
  title: "Titre d'evenement de la veille",
  description: "Ceci est une evenement d'une durée de 2h qui doit se dérouler la veille du jours d'aujourd'hui",
  owners: ["99999999999999", "organisateur2"],
  participants: ["participant1", "participant2", "participant3", "participant4"],
  color: "#263238",
  reccurent: {},
  url: `https://olympevueapp.onrender.com/user/99999999999999/events/${timestamp - milliParJour}`,
  public: true
}

let event2 = { id: timestamp + milliParJour, start: new Date(timestamp + milliParJour), end: new Date(timestamp + milliParJour + 60 * 60 * 1000 * 4), status: "confirmed", title: "Titre d'evenement du lendemain", description: "Ceci est une evenement d'une durée de 4h qui doit se dérouler le lendemain du jours d'aujourd'hui", owners: ["99999999999999", "organisateur2"], participants: ["participant1", "participant2", "participant3", "participant4"], color: "red", reccurent: {}, url: `https://olympevueapp.onrender.com/user/99999999999999/events/${timestamp + milliParJour}`, public: true }

let event3 = { id: timestamp + milliParJour * 4, start: new Date(timestamp + milliParJour * 4), end: new Date(timestamp + milliParJour * 4 + 60 * 60 * 1000 * 4), status: "confirmed", title: "Titre d'evenement plus tard", description: "Ceci est une evenement d'une durée de 4h qui doit se dérouler 4 jours apres aujourd'hui", owners: ["99999999999999", "organisateur2"], participants: ["participant1", "participant2", "participant3", "participant4"], color: "#0000FF", reccurent: {}, url: `https://olympevueapp.onrender.com/user/99999999999999/events/${timestamp + milliParJour}`, public: true }
let event4 = { id: timestamp + 100000*30 + milliParJour * 4, start: new Date(timestamp + 100000*30 + milliParJour * 4), end: new Date(timestamp + 100000*30 + milliParJour * 4 + 60 * 60 * 1000 * 4), status: "confirmed", title: "Titre d'evenement bien plus tard", description: "Ceci est une evenement d'une durée de 4h qui doit se dérouler 4 jours apres aujourd'hui", owners: ["99999999999999", "organisateur2"], participants: ["participant1", "participant2", "participant3", "participant4"], color: "pink", reccurent: {}, url: `https://olympevueapp.onrender.com/user/99999999999999/events/${timestamp + milliParJour}`, public: true }

let events = {}
events[event1.id] = event1
events[event2.id] = event2
events[event3.id] = event3
events[event4.id] = event4


let user = { id: "99999999999999", avatarURL: "https://avatarfiles.alphacoders.com/715/71560.jpg", username: "Utilisateur test", password: "password", email: "truc.bidule@test.org", team: ["team32145"], public: true, events, history: {}, }

new Event()

export default user



//exemple event google

/*{
  "id": string,
  "status": string,
  "created": datetime,
  "updated": datetime,
  "summary": string,
  "description": string,
  "location": string,
  "colorId": string,
  "creator": {
    "id": string,
    "email": string,
    "displayName": string,
    "self": boolean
  },
  "organizer": {
    "id": string,
    "email": string,
    "displayName": string,
    "self": boolean
  },
  "start": {
    "date": date,
    "dateTime": datetime,
    "timeZone": string
  },
  "end": {
    "date": date,
    "dateTime": datetime,
    "timeZone": string
  },
  "endTimeUnspecified": boolean,
  "recurrence": [
    string
  ],
  "recurringEventId": string,
  "originalStartTime": {
    "date": date,
    "dateTime": datetime,
    "timeZone": string
  },
  "transparency": string,
  "visibility": string,
  "iCalUID": string,
  "sequence": integer,
  "attendees": [
    {
      "id": string,
      "email": string,
      "displayName": string,
      "organizer": boolean,
      "self": boolean,
      "resource": boolean,
      "optional": boolean,
      "responseStatus": string,
      "comment": string,
      "additionalGuests": integer
    }
  ],
  "attendeesOmitted": boolean,
  "extendedProperties": {
    "private": {
      (key): string
    },
    "shared": {
      (key): string
    }
  },
  "hangoutLink": string,
  "conferenceData": {
    "createRequest": {
      "requestId": string,
      "conferenceSolutionKey": {
        "type": string
      },
      "status": {
        "statusCode": string
      }
    },
    "entryPoints": [
      {
        "entryPointType": string,
        "uri": string,
        "label": string,
        "pin": string,
        "accessCode": string,
        "meetingCode": string,
        "passcode": string,
        "password": string
      }
    ],
    "conferenceSolution": {
      "key": {
        "type": string
      },
      "name": string,
      "iconUri": string
    },
    "conferenceId": string,
    "signature": string,
    "notes": string,
  },
  "gadget": {
    "type": string,
    "title": string,
    "link": string,
    "iconLink": string,
    "width": integer,
    "height": integer,
    "display": string,
    "preferences": {
      (key): string
    }
  },
  "anyoneCanAddSelf": boolean,
  "guestsCanInviteOthers": boolean,
  "guestsCanModify": boolean,
  "guestsCanSeeOtherGuests": boolean,
  "privateCopy": boolean,
  "locked": boolean,
  "reminders": {
    "useDefault": boolean,
    "overrides": [
      {
        "method": string,
        "minutes": integer
      }
    ]
  },
  "source": {
    "url": string,
    "title": string
  },
  "attachments": [
    {
      "fileUrl": string,
      "title": string,
      "mimeType": string,
      "iconLink": string,
      "fileId": string
    }
  ]
}
*/