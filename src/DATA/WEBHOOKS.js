//TODO Faudrait passer par le serveur par securité

const WEBHOOKS = {}
//Acces a des WebHook
class WebHook {
  constructor(id, postURL) {
    this.id = id
    this.postURL = postURL
    this.active = true
    WEBHOOKS[this.id] = this
  }

  async getInfo() {
    return await fetch(this.postURL).then(async (res) => {
      let data = await res.json()
      this.webHookName = data.name
      this.channel_id = data.channel_id
      this.guild_id = data.guild_id
      return this
    })
  }

  async send(data) {
    if (!this.active) throw `Le webHook ${this.id} est désactivé`

    var options = {
      "method": "POST",
      "headers": { "content-type": "application/json" },
      "body": JSON.stringify(data)
    }
    console.log(`Envoie via le WebHook "${this.id}"`)
    return await fetch(this.postURL, options) //.then((res)=>{ return res.json()})

  }

  async sendMessage(content) {
    return await this.send({ "content": content })
  }


  async sendEmbed(title, fields, footer) {
    // var embed = {
    //   "embeds": [{
    //   "title": "TOP TEXT CHANGE THIS IN SCRIPT",
    //   "fields": [{
    //   "name": "test",
    //   "value": "content",
    //   "inline": false
    //   }],
    //   "footer": {
    //     "text": "BOTTOM TEXT CHANGE THIS IN SCRIPT"
    //   }
    //   }]
    // }
    return await this.send({ title, fields, footer })
  }
}


new WebHook("devbot", "https://discord.com/api/webhooks/735449213281894430/Y7_MWz355bexwHN3N_F3N3PAWmjKC6GP0iZkMmW4X2iVZIgp95Bn7pmn4rDmzL6Z42ml")
new WebHook("inscription", "https://discord.com/api/webhooks/737714272540426310/Ano04LUlAIA8Q0hgVyvRLl1deD4QBHR13IocJHAzLI6pU1FOFm-V2x0e11oxaylGmBz3")
new WebHook("olympe", "https://discord.com/api/webhooks/737724155595849749/x2E6DwFA_IlTCU0PcbGVBC5yH9B6QeMYQdvVaI80Mcp9iYBEzCz4CpWKx7IQCwecWtAh")
// Hook.sendMessage("On va pouvoir passer a la production en masse")

export default WEBHOOKS