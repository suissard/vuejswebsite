const API = {}

class Api {
  /**
   * Class gerant les api, leur parametres et les differentes requetes qui y sont liés
   * @param {*} id
   * @param {*} url
   * @param {*} port
   * @param {*} query
   * @param {*} header
   */
  constructor(id, url, port, query = {}, headers = {}) {
    this.id = id
    this.url = url
    this.port = port
    this.query = query //TODO potentiellement inutile
    this.headers = headers
    this.active = true //TODO useless ?
  

    API[this.id] = this
  }

  /**
   * Récupère des données a partir de l'API indiqué en GLOBALE
   * @param {*} path
   */
  async get(path, headers) {
    if (!this.active) throw `L'API ${this.id} est désactivé`
    if(!headers) headers= this.headers 
    let data = {  method: "GET", headers };
    let json = await fetch(`${this.url}${this.port?":"+this.port:""}/${path}`, data)
      .then(res => res.json())
      .catch(err => console.warn(err))
    return json
  }

  async post() {
    if (!this.active) throw `L'API ${this.id} est désactivé`

  }
  async delete() { }
  async put() { }
  async head() { }
  async connect() { }
  async option() { }
  async trace() { }
  async patch() { }

  
  toXForm(data) {
    return Object.keys(data)
      .map((key) => {
        return `${encodeURIComponent(key)}=${encodeURIComponent(data[key])}`;
      })
      .join("&");
  }


}

new Api("getibot", "http://82.64.182.198", "9999", { password: "A156d4SAD654d87fsfqs3d4AZ8574dqdED34344JhjbDJ34", userId: null, userToken: null, }, {})
new Api("postGeekingTime", "https://geekingtime.fr", "443")

class DiscordAPI extends Api{
  constructor(id, url, port, query = {}, headers = {}){
    super(id, url, port, query, headers)
  }

  checkToken(){
    if(!this.authorization_code) {
      console.log("Token discord inexistant")
      return false
    }

    if(this.authorization_code.error) {
      console.log("Error", this.authorization_code)
      return false
    }

    if(new Date().valueOf() > this.authorization_code.createAt + this.authorization_code.expires_in) {
      this.authorization_code = false
      console.log("Token discord périmé")
      return false
    }

    return true
  }

  async exchangeCode(code) {
    let data = {
      client_id: this.headers.clientId,
      client_secret: this.headers.clientSecret,
      grant_type: "authorization_code",
      code,
      redirect_uri: this.headers.redirectUrl,
      scope: this.headers.scope.join(" "),
    };

    await fetch(`${this.url}/oauth2/token`, { headers: {"Content-Type": "application/x-www-form-urlencoded"}, method: "POST", body: this.toXForm(data) }).then(async (r) => {
      this.authorization_code = await r.json();
      this.headers["Authorization"] = `${this.authorization_code.token_type} ${this.authorization_code.access_token}`
      this.authorization_code.createAt = new Date().valueOf()
      console.log("exchangeCode OK", this.authorization_code)
    });

    if(this.checkToken())return true
    return false
  }

  async refreshToken() {
    if (!this.authorization_code) throw "No authorization_code"

    let data = {
      client_id: this.headers.clientId,
      client_secret: this.headers.clientSecret,
      grant_type: "refresh_token",
      refresh_token: this.authorization_code.refresh_token,
      redirect_uri: this.headers.redirectUrl,
      scope: this.headers.scope.join(" "),
    };

    await fetch(`https://discord.com/api/oauth2/token`, { headers: {"Content-Type": "application/x-www-form-urlencoded"}, method: "POST", body: this.toXForm(data) }).then(async (r) => {
      this.authorization_code = await r.json();
      this.headers["Authorization"] = `${this.authorization_code.token_type} ${this.authorization_code.access_token}`
      this.authorization_code.createAt = new Date().valueOf()
      console.log("refreshToken OK")

    });
    if(this.checkToken())return true
    return false
  }

}
new DiscordAPI("discord", "https://discordapp.com/api",undefined, {}, { "Access-Control-Allow-Origin":"*","Content-Type": "application/x-www-form-urlencoded", clientId: "716968666821820497", clientSecret: "8Al0dg2Rgcj8p9igSJwyHzVGQt5kJcWX", redirectUrl: "DEFINIT AU DEMARRAGE DANS $app.created", scope: [ "identify","email","guilds", "connections"] })

export default API


/*
TODO
Api Olympe
*/ 