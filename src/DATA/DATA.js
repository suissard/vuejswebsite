const DATA = {}

/**
 * Class des collections des données du site (Générale)
 */
class DataCollection extends Map{
  constructor(type, api){
    if(!type || !api) throw new TypeError("Il manque un argument pour la création de la collection")
    
    super()
    DATA[type]=this
    this.type = type
    this.api = api
  }
  
  setSource(api){
    this.api = api
  }
  
  /**
   * Charger les données completes ou seulement une partir en fonction des arguments
   * @param {Array} entries Array avec els entrées a charger en plus des identifiants
   * @param {Number|String} start numero ou string indiquand le point de départ des données a retourner
   * @param {Number|String} end numero ou string indiquand le point final des données a retourner
   */
  fetch(entries, start, end){
    console.lol(entries, start, end)
  }

  
  /**
   * Charge progressivement les données dans une cible (TODO doit permettre la reactivité dans les composants Vue)
   * @param {*} target Reference a un objet cible ou fonction pour enrichir cette cible
   */
  async progressiveFetchAll(target, entries = ["id"], start, end){
    let liste = await this.fetch(entries, start, end)
    for (let entrie of liste){
      target = await entrie.fetch()
      // ou
      // target(await entrie.fetch())
    }
  }

}



/**
 * Class des données du site (Générale)
 */
 
class Data {
  constructor(type, source){
    this.type = type
    this.source = source //Objet contenant l'adresse des requetes et les options de bases. Par defaut celle de la DataCollection
  
  }
  
  async fetch(){
    return await fetch(this.source.url, this.source.options).json();
  
  
  }
}


class Game extends Data {
  constructor(){
      super("Game")
      
    }
    
    
    
}

new DataCollection(true, true)
new Game


export default DATA