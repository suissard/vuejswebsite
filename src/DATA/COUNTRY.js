//Info sur les pays/langues disponibles
const COUNTRY = {
  fr: { id: "fr", countryName: "France", emote: "🇫🇷", active: true },
  uk: { id: "uk", countryName: "United Kingdom", emote: "🇬🇧", active: true },
  ch: { id: "ch", countryName: "China", emote: "🇨🇳", active: true },
  us: { id: "us", countryName: "United States", emote: "🇺🇸", active: true },
  xx: { id: "xx", countryName: "Chtulutien", emote: "🐙", active: true }
}

import langageComponents from './COUNTRY/Components.json'
/**
 * CETTE FONCTION EST EN WIP/TEST
 * Charger la langue demandé et rafraichir/recalculer le composant
 * @param {*} component 
 * @param {*} ref 
 */
let asyncLoadLangage = async function (component, ref) {
  //TODO fonction de traduction automatique via API ?
  //let result = await langageComponents.fetch(ref, component.$app.country)
  await setTimeout(() => {
    let langage = component.$app.country
    langageComponents["xx"] = {}
    langageComponents["xx"][ref] = {
      h3_1: "gbsdfvgsdfhbvsdb",
      label_1: "hk,jkl;mhgjkm",
      label_2: "gsfdgb sdgvsq",
      h3_2: "gfjhgfhkjg",
      label_3: "qqysertser",
      label_4: "rsgnrsgrqen",
      label_5: "grsdgn q",
      label_6: "ojhpkhbjn",

      btn_1: "jjqfqz",
      btn_2: "ihghghbhjbjhb"
    }
    // utiliser la reactivité des donnée pour mettre a jour le computed
    component.$app.country = " "
    component.$app.country = langage
  }, 2000)
}


/**
 * Renvoie le contenue pour el composant, avec la langue déclaré dans $app
 * @param {*} component 
 */
var multiLangage = function (component, ref) {
  let result = langageComponents[component.$app.country]
  if (!result) { //Si il n'ya pas de langue chargé on lance la fonction de chargement asynchrone
    component.$app.notif({ title: "Changement de langue", type: "warning", timer: 5 })
    asyncLoadLangage(component, ref)
    return undefined
  } else if (result[ref]) {
    return result[ref]
  } else if (langageComponents["fr"][ref]) {
    return langageComponents["fr"][ref]
  }
  return undefined
}

export default { COUNTRY, multiLangage }



/*
TODO 
Voir code de Tristan 
pas de trad a chaud par contre 
*/