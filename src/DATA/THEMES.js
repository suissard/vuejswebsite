//Définition du theme principal
import colors from 'vuetify/lib/util/colors'

let AFO = {
  id: "AFO",
  appTitle: "AllForOne",
  logoURL: "/logoAFO.png",
  backgroundURL: "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/99dd2a00-9374-49ca-9871-223164cecd85/dc6nir0-708829cd-8b31-4938-a403-5fe2f59e2c31.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3sicGF0aCI6IlwvZlwvOTlkZDJhMDAtOTM3NC00OWNhLTk4NzEtMjIzMTY0Y2VjZDg1XC9kYzZuaXIwLTcwODgyOWNkLThiMzEtNDkzOC1hNDAzLTVmZTJmNTllMmMzMS5qcGcifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6ZmlsZS5kb3dubG9hZCJdfQ.hYMBB6rhwdPU9IYez-FUN2of7ArMh8sNoxwAQDVut1I",
  reseauxSocio: {
    twitterFR: {
      href: "https://twitter.com/PlayAllForOneFR",
      text: "Twitter 🇫🇷",
      icon: "twitter",
    },
    twitterUK: {
      href: "https://twitter.com/PlayAllForOneUK",
      text: "Twitter 🇬🇧",
      icon: "twitter",
    },
    facebookFR: {
      href: "https://twitter.com/PlayAllForOneFR",
      text: "Facebook 🇫🇷",
      icon: "facebook",
    },
    facebookUK: {
      href: "https://twitter.com/PlayAllForOneFR",
      text: "Facebook 🇬🇧",
      icon: "facebook",
    },
  },

  light: {
    primary: colors.blueGrey.lighten4, accent: colors.amber.accent3,
  },
  dark: {
    primary: colors.blueGrey.darken4, secondary: colors.amber.darken3, accent: colors.amber.accent3,
  },
}

let geekingtime = {
  id: "geekingtime",
  appTitle: "Geeking Time",
  logoURL: "https://geekingtime.fr/wp-content/uploads/2020/07/Logo-GETI-Carr%C3%A9-20.png",
  backgroundURL: "https://geekingtime.fr/wp-content/uploads/2020/07/Background-GETI-1.jpg",
  reseauxSocio: {
    twitter: {
      href: "https://twitter.com/geekingtime",
      text: "Twitter",
      icon: "twitter",
    },
    instagram: {
      href: "https://instagram.com/geekingtime",
      text: "Instagram",
      icon: "instagram",
    },
    facebook: {
      href: "https://facebook.com/geekingtime",
      text: "Facebook",
      icon: "facebook",
    },
  },
  dark: {
    primary: "#390D53",
    secondary: "#B3781A",
    accent: "#FF4700",
    error: "#FF5252",
    info: "#2196F3",
    success: "#4CAF50",
    warning: "#FB8C00"
  },
  light: {
    primary: "#AE92BE",
    secondary: "#BB9928",
    accent: "#68520B",
    error: "#FF5252",
    info: "#2196F3",
    success: "#4CAF50",
    warning: "#FB8C00"
  }
}

const THEMES = {AFO, geekingtime}
export default THEMES


