//simulation de chargemetn de donnée statique ()
const GLOBAL = {}
// import Vue from 'vue'
// Vue.prototype.$app = GLOBAL

//Outils de gestion des données
import DATA from './DATA/DATA.js'
GLOBAL.DATA = DATA

import THEMES from './DATA/THEMES.js'
GLOBAL.THEMES = THEMES
GLOBAL.THEME = THEMES.AFO

// import COOKIES from './DATA/COOKIES.js'
// GLOBAL.COOKIES = COOKIES

import PRN from './pages/PAGES.js'
GLOBAL.PAGES = PRN.PAGES
GLOBAL.ROUTES = PRN.ROUTES


import COUNTRY from './DATA/COUNTRY.js'
GLOBAL.COUNTRY = COUNTRY.COUNTRY
GLOBAL.multiLangage = COUNTRY.multiLangage

import API from './DATA/API.js'
GLOBAL.API = API

import WEBHOOKS from './DATA/WEBHOOKS.js'
GLOBAL.WEBHOOKS = WEBHOOKS

// import TEST from './DATA/TEST.js'
// GLOBAL.USER = TEST




export default GLOBAL