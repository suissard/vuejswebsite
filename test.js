let scoreWin = 5
let scoreLoose = 0
let scoreEqual = 2

class Tournoi {
  constructor(tours, nbrTeams) {

    this.tours = tours
    this.nbrTeams = nbrTeams

    this.teams = new Map()
    this.format = "Round suisse"
    this.tourEnCours = 1
    /**
     * Repertorier par tour
     * * Liste des matchs
     * * Erreurs (repetition d'adversaire, abscence d'aversaire en situation de nombre d'équipes pairs)
     * * Distribution des scores
     */
    this.log

    this.start(this.tours, this.nbrTeams)
  }


  createTeam(i) {
    return { name: `team${i}`, score: 0, matchHistory: [], paired, resultHistory: [], elo: this.nbrTeams - i, randomSeed: Math.floor(Math.random() * (this.nbrTeams * 10)) }
  }

  // team Object
  teamsObject() {
    let teams = {}
    for (let i = 1; i <= this.nbrTeams; i++) {
      teams[`team${i}`] = this.createTeam(i)
    }
    return teams
  }

  // team Array
  teamsArray() {
    let teams = []
    for (let i = 1; i <= this.nbrTeams; i++) {
      teams.push(this.createTeam(i))
    }
    return teams
  }

  // team Map
  teamsMap() {
    this.teams = new Map()
    for (let i = 1; i <= this.nbrTeams; i++) {
      this.teams.set(`team${i}`, this.createTeam(i))
    }
    return this.teams
  }

  //trier équipes en fonction de certaines variables
  sortTeams(teams, croissant = true, entrie = "score") {
    function compareNombres(a, b) {
      if (croissant) return a[entrie] - b[entrie];
      if (!croissant) return b[entrie] - a[entrie];
    }
    teams.sort(compareNombres)
    // for (i in teams){
    //   console.log(teams[i].randomSeed)
    // }
    return teams
  }

  /**
   * simule une rencontre entre deux team
   * @param {*} team1
   * @param {*} team2
   * @param {*} VDE 1 ne feras que des victoires pour la team 1, 2 feras des victoires ou des defaites au hasard, 3 feras victoire egalité defaite au hasard
   */
  functionMatch(team1, team2, VDE = 3) {
    // determiner la victoire
    let result = Math.floor(Math.random() * VDE)

    if (!team2) {
      team1.score += scoreWin
      team1.matchHistory.push("NOTEAM" + Math.floor(Math.random() * 10000))
      team1.resultHistory.push("Victoire")
      return
    } else {

      // modifier les objets teams
      switch (result) {
        case 0: {
          team1.score += scoreWin
          team2.score += scoreLoose
          result = "Victoire"
          break
        }
        case 1: {
          team2.score += scoreWin
          team1.score += scoreLoose
          result = "Défaite"
          break
        }
        case 2: {
          team1.score += scoreEqual
          team2.score += scoreEqual
          result = "Egalité"
          break
        }
      }

      team1.matchHistory.push(team2.name)
      team1.resultHistory.push(result)

      team2.matchHistory.push(team1.name)
      team2.resultHistory.push(result)

    }
  }

  /**
   * simuler tour de ronde suisse
   * @param {*} teams equipes trié dasn l'ordre
   * @param {*} tour indique quel tour est simulé
   */
  OLDtourRondeSuisse(TEAMS) {
    // trier
    let teams = this.sortTeams(TEAMS.map((value, index) => { return value }))

    //dérouler les teams et les grouper deux par deux et les retirer du tableau au fur et a mesure
    for (let i = 0; teams.length > 1; i++) {
      let team1 = teams.splice(0, 1)[0], team2

      //=====================================================================================================
      //definir la team 2 en faisant exceptions pour les teams qui se sont déjà rencontrées
      for (let ii in teams) {
        if (!team1.matchHistory.includes(teams[ii].name)) { team2 = teams.splice(ii, 1)[0]; break }
      }
      //=====================================================================================================

      this.functionMatch(team1, team2)
    }
    return TEAMS
  }

  tourRondeSuisse(TEAMS) {
    // trier et reférencer par score
    let scoreCluster = {}
    let teams = this.sortTeams(TEAMS.map((value, index) => {
      if (!scoreCluster[value.score]) { scoreCluster[value.score] = [value] } else { scoreCluster[value.score].push(value) }
      return value
    }))

    //donne un score d"appairage au sein du cluster, pour anticiper ceux qui seront le plus dur a recycler ailleurs
    function appairageScore(team, cluster) {
      let result = 0
      for (let i in cluster) {
        if (cluster[i].matchHistory.includes(team.name)) result++
      }
      team.appairageScore = result
      return result
    }

    //faire des pairs au sein d'un cluster
    function pairCluster(cluster) {
      let pair, alone
      for (let i in cluster) {
        appairageScore(cluster[i], cluster)
      }

        //trier par score d'apairage pour se debarraser des cas les plus dur a appairer
        this.sortTeams(cluster.map((value, index) => { return value }), false, "cluster")



      this.functionMatch(team1, team2)

      if (cluster.length % 2 == 1)console.log("cluster impair")

      return { pair, alone }
    }

    //dérouler les teams et les grouper deux par deux et les retirer du tableau au fur et a mesure
    for (let i = 0; teams.length > 1; i++) {
      let team1 = teams.splice(0, 1)[0], team2



      //=====================================================================================================
      //definir la team 2 en faisant exceptions pour les teams qui se sont déjà rencontrées
      for (let ii in teams) {
        if (!team1.matchHistory.includes(teams[ii].name)) { team2 = teams.splice(ii, 1)[0]; break }
      }
      //=====================================================================================================



      this.functionMatch(team1, team2)
    }
    return TEAMS
  }

  /**
   * Met en valeur les données pour relever les erreurs
   * @param {*} teams
   */
  checkError(teams, nbrTours) {
    let distributionScore = {}
    this.sortTeams(teams, false)
    for (let i in teams) {
      let team = teams[i]
      if (!distributionScore[team.score]) { distributionScore[team.score] = 1 } else { distributionScore[team.score]++ }

      //verifier les Doublons dans l'historique
      if (team.matchHistory.length != Array.from(new Set(team.matchHistory)).length) console.log(`DOUBLON : ${team.name} - ${team.score} - (${team.matchHistory.length})[${team.matchHistory.join(", ")}]`)

      //verifier la longueur de l'historique
      if (team.matchHistory.length != nbrTours) console.log(`WRONG HISTORIQUE : ${team.name} - ${team.score} - (${team.matchHistory.length})[${team.matchHistory.join(", ")}]`)
    }
    //logger la distribution
    console.log(JSON.stringify(distributionScore))
  }

  /**
   * Simule un tournoi ronde suisse
   * @param {*} nbrTours nombre de tour a générer
   * @param {*} nbrTeams nombre de team a generer
   */
  start(nbrTours, nbrTeams) {

    this.teams = this.teamsMap()
    // console.log(1, this.teams.get("team3"))

    // let TEAMS = this.teamsArray(nbrTeams)
    let TEAMS = []
    this.teams.forEach((value) => { TEAMS.push(value) })

    let startTournament = new Date().valueOf()

    while (this.tourEnCours <= nbrTours) {
      let startTour = new Date().valueOf()
      TEAMS = this.tourRondeSuisse(TEAMS)
      this.tourEnCours++

    }

    console.log(1, this.teams.get("team3"))


    console.log(`Tournoi avec ${TEAMS.length} teams et ${nbrTours} tours simulés en ${new Date().valueOf() - startTournament} millisecondes`)
    this.checkError(TEAMS, nbrTours)
  }

}
//================================================================

let tours = 13, nbrTeams = 16
let tournoiGeti = new Tournoi(tours, nbrTeams)

console.log(tournoiGeti.teamsMap().size)

// tournoiGeti.tournoiRondeSuisse(13, 32)
// tournoiGeti.tournoiRondeSuisse(13, 32)
// tournoiGeti.tournoiRondeSuisse(13, 32)
// tournoiGeti.tournoiRondeSuisse(13, 32)
// tournoiGeti.tournoiRondeSuisse(4, 16000)
// tournoiGeti.tournoiRondeSuisse(4, 16000)
// tournoiGeti.tournoiRondeSuisse(4, 16000)
// tournoiGeti.tournoiRondeSuisse(4, 16000)
// tournoiGeti.tournoiRondeSuisse(4, 16000)