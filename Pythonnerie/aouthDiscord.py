import requests
import json

from urllib.parse import quote

API_ENDPOINT = 'https://discordapp.com/api'
CLIENT_ID = '716968666821820497'
CLIENT_SECRET = '8Al0dg2Rgcj8p9igSJwyHzVGQt5kJcWX'
REDIRECT_URI = 'https://olympevueapp.onrender.com/auth'
PUBLIC_KEY = 'e74d333048e11ff0b0171dfd7d7d37e5b1fed8870fa6918a16663616b8ac7320'

def toXForm(data):
    result = []
    for item in data:
        result.append(quote(str(item)) +'='+ quote(str(data[item])))
    return "&".join(result)


def exchange_code(code):
    data = {
        'client_id': CLIENT_ID,
        'client_secret': CLIENT_SECRET,
        'grant_type': 'authorization_code',
        'code': code,
        'redirect_uri': REDIRECT_URI,
        'scope': "identify email guilds connections"
    }
    headers = {
    #    "Access-Control-Allow-Origin":"*",
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    r = requests.post(API_ENDPOINT +'/oauth2/token', data=json.dumps(data), headers=headers,)
    r.raise_for_status()
    return r.json()

result = exchange_code('sTJsOVcqJmcQic0uwUpueNrULYRubG')
print(result)
#https://discord.com/oauth2/authorize?client_id=716968666821820497&redirect_uri=http://localhost:8080/auth&response_type=code&scope=identify%20email%20guilds%20connections