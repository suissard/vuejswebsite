# import numpy as np
# class Map :
#     def __init__(self, liste=[]):
#         self.__data = {} #est ce qu'on a pas envie de le mettree au format list pour conserver l'ordre
#         for item in liste:
#             self.set(item["id"], item)
    
#     def set(self, id, content):
#         self.__data[id] = content
#         return self.__data[id]

#     def get(self, id):
#         return self.__data[id]
        

class Rencontre:
    '''
    Objet definissant une rencontre et ses méthodes

    Parameters
    ----------
        opposants : ``list`` - Liste des opposants s'afrontant dans cette rencontre

        startTime : ``timestamp`` - horaire de départ de la rencontre

        endTime : ``timestamp`` - horaire de fin de la rencontre

        maps : ``list`` - Caractéristiques des maps de la rencontrent
        
        conditions : ``list`` - Liste des conditions d'affrontement de cette rencontre

    '''

    def __init__(self, opposants, startTime, endTime, maps={}, conditions=[]):
        self.__opposants = opposants # private
        self.startTime = startTime
        self.endTime = endTime
        self.maps = maps
        self.conditions = conditions
 
    def setOpposant (self, opposant) :
        self.__opposants[opposant.id] = opposant
        return opposant
    
    def getOpposant (self, opposantId) :
        return self.__opposants[opposantId]



    def set_score(self, opposantId, score):
        '''

        '''
        if not self.__opposants.get(opposantId):raise ValueError("Il n'y a pas d'opposant avec cette ID")
        self.__opposants.get(opposantId).score = score
        return self.__opposants.get(opposantId)


#test = Rencontre(map(print, "tres"), 123, 123).set_score(False, False)
#test.set_score()

#test = [{"id":1, "content":"test"}, {"id":2, "content":"iughiuh"}, {"id":3, "content":"ohlkjhk"}]
#print(test[1])