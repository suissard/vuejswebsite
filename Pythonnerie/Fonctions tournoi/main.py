from decorator_timer import timer
from Simulation import  Simulation

####################################################################
@timer
def simulation(nbrOpponents: int, nbrTurns: int = 4, slotMeeting: int = 2, filters=['decroissant', 'noRematch']):
    return Simulation(nbrOpponents,nbrTurns)
####################################################################

test = simulation(200, 199)

pass