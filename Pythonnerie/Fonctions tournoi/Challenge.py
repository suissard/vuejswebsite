import collections
import time
from math import floor, ceil
from decorator_timer import timer

from Meeting import MeetingsManager, Meeting
from Opponent import OpponentsManager, Opponent
from Connector import ConnectorsManager, Connector


class TurnsManager():
    """"""
    def __init__(self):
        self.__turns = collections.OrderedDict()

    def set_turn(self, id: str, meetings):
        if self.get_turn(id):
            raise Exception("L'id '{}' existe déjà".format(id))
        self.__turns.__setitem__(id, Turn(meetings))
        return self.get_turn(id)

    def get_turn(self, id: str):
        return self.__turns.get(id)

    def remove_turn(self, id: str):
        return self.__turns.pop(id)

    def getAll_turns(self):
        return self.__turns

    def reset_turns(self):
        self.__turns = collections.OrderedDict()

class Turn(MeetingsManager):
    def __init__(self, meetings):
        super().__init__()
        for meet in meetings:
            self.set_meeting(meet.id, meet)




class Manager(OpponentsManager, MeetingsManager, ConnectorsManager, TurnsManager):
    def __init__(self):
        OpponentsManager.__init__(self)
        MeetingsManager.__init__(self)
        ConnectorsManager.__init__(self)
        TurnsManager.__init__(self)
        pass
        

    def reset_all(self):
        self.reset_opponents()
        self.reset_meetings()
        self.reset_connectors()
        self.reset_turns()


class Challenge(Manager):
    """"""
    def __init__(self, id:str, name:str, format:str="pool", slot:int = 2, filters:list=["decroissant"]):
        Manager.__init__(self)
        self.id = id
        self.name = name
        self.format = format
        self.slot = slot #ca impliquerai que tout les slot soit les meme
        self.filters = filters
        
    #@abstractmethod
    def drawPattern(self, nbrTurns:int):
        raise Exception("Cette methode doit etre surchargée")

    def createMeetings(self, slot:int, nbrMeeting:int=1, startTime:int=time.time(), endTime:int=time.time()+3600):
        '''Creation des meetings et remplissage avec des opponents'''
        result=[]
        for i in range(nbrMeeting):
            meet = Meeting(self.getAll_meetings().__len__(), self.slot, startTime, endTime)
            result.append(self.set_meeting(meet.id, meet))
            
        return result


    def fillOpponents(self):        
        listing = self.getAllMatchs()
                
        for i in self.getAll_meetings():
            meet = self.get_meeting(i)
            meet.reset_opponents()
            if listing.__len__()>0:opponents = listing.pop(0)
            else: break
            for oppo in opponents:
                meet.add_opponent(oppo)
        pass

    def getAllMatchs(self):
        """renvoie la liste des opposants dans l'ordre où ils doivent remplir les meetings du Challenge"""
        result = []
        for i in range(ceil(self.getAll_opponents().__len__()/self.slot)):
            match = ()
            for ii in range(self.slot):
                match+= (self.get_opponent(i*self.slot+ii),)
            result.append(match)
        return result

class RoundRobin(Challenge):
    @timer
    def drawPattern(self, nbrTurns:int):
        """Effacement des précedente données et creation des meeting et connector necessaire a la realisation du tournoi, proportionnellement aux nombre d'opposants"""
        self.reset_meetings()
        self.reset_connectors()
        self.reset_turns()

        for i in range(nbrTurns):
            self.set_turn("turn"+str(i+1), self.createMeetings(self.slot, ceil(self.getAll_opponents().__len__()/self.slot)))
            
            if i == 0: continue
            connector = self.set_connector("turn" +str(i)+">"+str(i+1),"turn" +str(i)+">"+str(i+1), self.id)
            connector.input.add_meeting(self.get_turn("turn"+str(i)).getAll_meetings())
            connector.output.add_meeting(self.get_turn("turn"+str(i+1)).getAll_meetings())
        #Output self.get_turn("turn"+str(i+1))

        return self.getAll_turns()

    def getAllMatchs(self):
        if self.getAll_opponents().__len__() % 2 == 1: return self.roundRobinImpair()
        else: return self.roundRobinPair()

    def roundRobinPair(self):
        #code recuperé sur : https://codereview.stackexchange.com/questions/217969/simulate-round-robin-tournament-draw
        """Return the list of games with pair team number."""
        matches = []
        half_len = int(len(self.getAll_opponents())/2)
        arr1 = [i for i in range(half_len)]
        arr2 = [i for i in range(half_len, len(self.getAll_opponents()))][::-1]
        for i in range(len(self.getAll_opponents())-1):
            arr1.insert(1, arr2.pop(0))
            arr2.append(arr1.pop())
            for a, b in zip(arr1, arr2):
                matches.append((self.get_opponent(a), self.get_opponent(b)))
        return matches

    def roundRobinImpair(self):
        #code recuperé sur : https://codereview.stackexchange.com/questions/217969/simulate-round-robin-tournament-draw
        """Return the list of games with impair team number."""
        matches = []
        half_len = int((len(self.getAll_opponents())+1)/2)
        arr1 = [i for i in range(half_len)]
        arr2 = [i for i in range(half_len, len(self.getAll_opponents())+1)][::-1]
        for i in range(len(self.getAll_opponents())):
            arr1.insert(1, arr2.pop(0))
            arr2.append(arr1.pop())
            for a, b in zip(arr1, arr2):
                if len(self.getAll_opponents()) not in (a, b):
                    matches.append((self.get_opponent(a), self.get_opponent(b)))
        return matches
