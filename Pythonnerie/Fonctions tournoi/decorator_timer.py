import functools
import time
from math import floor


def timer(func):
    """Timestamp decorator for dedicated functions"""
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        elapsed = floor((time.time() - start)*1000)/1000

        hours = floor(elapsed/(60*60))
        minutes = floor((elapsed - (hours*60*60))/60)
        secondes = floor(elapsed - (hours*60*60) - (minutes*60))
        milli = floor((elapsed - (hours*60*60) - (minutes*60) - secondes)*1000)

        if hours > 0: hours = str(hours) + "h"
        else: hours = ""
        if minutes > 0: minutes = str(minutes) + "m"
        else: minutes = ""
        if secondes > 0 and hours == "": secondes = str(secondes) + "s"
        else: secondes = ""
        if milli > 0 and minutes == "": milli = str(milli)
        else: milli = ""

        print('"{}" a durée {} '.format(func.__name__, hours + minutes + secondes + milli))

        return result
    return wrapper
