from cmp_to_key import cmp_to_key
from decorator_timer import timer
from random import shuffle, randint

#TODO faire des class pour chaque 
class Decroissant():
    def __init__(self,opponentsInput:list, attribut=lambda x: x.score)->list:
        '''Trier par ordre décroissant'''
        sorted(opponentsInput, key=cmp_to_key(lambda x, y: attribut(y) - attribut(x)))

class Croissant():
    def __init__(self,opponentsInput:list, attribut=lambda x: x.score)->list:
        '''Trier par ordre croissant'''
        sorted(opponentsInput, key=cmp_to_key(lambda x, y: attribut(x) - attribut(y)))

#===========================================================================================================

def decroissant(opponentsInput: list, attribut=lambda x: x.score) -> list:
    '''Trier par ordre décroissant'''
    return sorted(opponentsInput, key=cmp_to_key(lambda x, y: attribut(y) - attribut(x))), None


def croissant(opponentsInput: list, attribut=lambda x: x.score) -> list:
    '''Trier par ordre croissant'''
    return sorted(opponentsInput, key=cmp_to_key(lambda x, y: attribut(x) - attribut(y))), None


@timer
def noRematch(opponentsInput: list, attribut=None,) -> list:
    '''Repartir de manière a avoir une liste de couple d'opposant qui ne se sont pas déjà rencontré'''
    class Couplage:
        """
        Permet de coupler une liste opposants en fonction de leur historique de rencontre
        """
        def __init__(self, opponentslist: list):
            self.opponentslist = opponentslist

            self.tryCouplage = []
            self.listCouples = []
            self.sortedOpponentsIdList = None
            self.availableOpponents = None
            self.reste = []

        def findAllCouples(self):
            while self.sortedOpponentsIdList.__len__() > 0:
                self.sortedOpponentsIdList[0]
                self.findCouple(self.sortedOpponentsIdList.pop(0))

            if self.reste.__len__() == 0: return
            print('Il reste {} opposants sans adversaires'.format(self.reste.__len__()))
            if self.reste in self.tryCouplage: 
                print("On a déjà tout essayé")

            self.tryCouplage.append(self.reste)
            self.sortedOpponentsIdList = self.reste + self.listCouples
           
            #TODO Methode de trie progressive : merge simple de reste et couple, decalage de 1, random
            #Decaler de 1
            self.sortedOpponentsIdList.append(self.sortedOpponentsIdList.pop(0) )
            #Randomiser
            #shuffle(self.sortedOpponentsIdList)
            self.listCouples = []
            self.reste = []
            self.findAllCouples()
            


        def findCouple(self, first):
            second = None
            for i, oppoId in enumerate(self.sortedOpponentsIdList):
                if first in self.availableOpponents[oppoId]['available']:
                    second = self.sortedOpponentsIdList.pop(i)
                    self.listCouples += [first, second]
                    break
            if second == None:
                self.reste.append(first)
                #print("Pas d'opposant trouvé pour la team "+str(first))
                return
            # print("Couple trouvé entre {} et {}".format(first, self.second))

        def couplesToList(self) -> list:
            resultCouples = []
            resultReste = []
            
            for oppoId in self.listCouples:
                resultCouples.append(self.availableOpponents[oppoId]['opponent'])
            
            for oppoId in self.reste:
                resultReste.append(self.availableOpponents[oppoId]['opponent'])
            
            return resultCouples, resultReste

        def appairingScore(self, opponent):
            available = []
            for oppo in self.opponentslist:
                if opponent.id == oppo.id or [opponent.id] in oppo.getHistoriqueOpponents(): continue
                available.append(oppo.id)
            return {'available': available, 'opponent': opponent}        

        def appairingScores(self):
            self.availableOpponents = {}
            for oppo in self.opponentslist:
                self.availableOpponents[oppo.id] = self.appairingScore(oppo)

            self.sortedOpponentsIdList = croissant(self.availableOpponents, lambda x: self.availableOpponents[x]['available'].__len__())

    couplage = Couplage(opponentsInput)

    couplage.appairingScores()
    couplage.findAllCouples()    

    # TODO Verifier la qualité du couplage
    # TODO Eviter que le couplage redémarre en boucle
    #for oppo in result: print("{}-{} : {}".format(oppo.id, oppo.score, oppo.getHistoriqueOpponents()))

    return couplage.couplesToList()


def randomize(opponentsInput:list)->list:
    """randommise l'ordre d'une list"""
    shuffle(opponentsInput)
    return opponentsInput


def oneSurvivor():
    pass

