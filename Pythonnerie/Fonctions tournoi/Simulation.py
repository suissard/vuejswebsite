import Challenge
from Opponent import Opponent
from decorator_timer import timer

class Simulation(Challenge.RoundRobin):
    def __init__(self, nbrOpponents, nbrTurns, filters=["random"], format="pool", id:str="testId", name:str="testName", slot:int = 2):
        Challenge.RoundRobin.__init__(self, id, name, slot)
        self.start(nbrOpponents, nbrTurns)

    def generateScore(self, meetings, funcScore=lambda num: (num%2)*3 ):
        '''SIMULATION : Genere un score pour tout les opposants présents dans la liste des meetings en input'''
        for i in meetings:
            meet = meetings[i]
            meet.setOpponentsHistory()
            for ii in meet.getAll_opponents():
                meet.add_score(ii, funcScore(ii))
            meet.state = "close"

    @timer
    def createOpponents(self, nbrOpponents: int):
        '''Creation d'une liste ordonnée d'opponents'''
        for i in range(nbrOpponents):
            opponent = Opponent(i, "Team"+str(i+1), "testChallengeId")
            self.set_opponent(opponent.id, opponent)

    def printCouplePerTurns(self):
        couples = []
        error = []
        def couplePerTurns(ii):
            id1 = str(self.get_turn(i)[ii].get_opponent(0).id)
            id2 = str(self.get_turn(i)[ii].get_opponent(1).id)

            if (id1, id2) in couples or (id2, id1) in couples : error.append((id1, id2))
            couples.append((id1, id2))

            if not (id1, id2) in error: return id1 + "-" + id2
            else: return id1 + "X" + id2

        for i in self.getAll_turns():
            print(str(i) + " == "+ " | ".join(map(couplePerTurns,self.get_turn(i))))

    def start(self, nbrOpponents:int=32, nbrTurn:int=4, slotMeeting:int=2):
        '''WIP Démarrage de la simulation'''

        print('Tache démarré : {} opposants sur {} tours'.format(nbrOpponents, nbrTurn))

        self.createOpponents(nbrOpponents)
        self.drawPattern(nbrTurn)
        self.fillOpponents()

        for i in self.getAll_connector():
            connector = self.get_connector(i)
            self.generateScore(connector.input.getAll_meetings())
        self.generateScore(connector.output.getAll_meetings())

        # + Creer des filtres et fonctions de trie

        # + simuler round suisse, bracket etc

        print('Tache terminée : {} opposants et {} rencontres sur {} tours'.format(
            self.getAll_opponents().__len__(), self.getAll_meetings().__len__(), nbrTurn
            ))