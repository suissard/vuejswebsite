#code recuperé sur : https://codereview.stackexchange.com/questions/217969/simulate-round-robin-tournament-draw

class roundRobin:
    def __init__(self, teams:list):
        self.teams = teams
        self.matches = []

    def fillOpponents(self):
        if self.teams.__len__() % 2 == 1: self.roundRobinImpair()
        else: self.roundRobinPair()


    def roundRobinPair(self):
        """Return the list of games with pair team number."""
        half_len = int(len(self.teams)/2)
        arr1 = [i for i in range(half_len)]
        arr2 = [i for i in range(half_len, len(self.teams))][::-1]
        for i in range(len(self.teams)-1):
            arr1.insert(1, arr2.pop(0))
            arr2.append(arr1.pop())
            for a, b in zip(arr1, arr2):
                self.matches.append((self.teams[a], self.teams[b]))
        return self.matches

    def roundRobinImpair(self):
        """Return the list of games with impair team number."""
        half_len = int((len(self.teams)+1)/2)
        arr1 = [i for i in range(half_len)]
        arr2 = [i for i in range(half_len, len(self.teams)+1)][::-1]
        for i in range(len(self.teams)):
            arr1.insert(1, arr2.pop(0))
            arr2.append(arr1.pop())
            for a, b in zip(arr1, arr2):
                if len(self.teams) not in (a, b):
                    self.matches.append((self.teams[a], self.teams[b]))
        return self.matches