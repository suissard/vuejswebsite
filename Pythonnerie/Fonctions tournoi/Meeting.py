from decorator_timer import timer

from datetime import datetime
from Opponent import OpponentsManager
import collections

class Meeting(OpponentsManager):
    '''
    Objet definissant une meeting et ses méthodes

    Parameters
    ----------
        slot : ``int`` - Nombre de slot max d'opponents

        startTime : ``timestamp`` - horaire de départ de la meeting

        endTime : ``timestamp`` - horaire de fin de la meeting. Doit être supérieur a startTime

        state : ``string`` - Etat de la meeting : 
            * open : La meeting est ouverte a l'entrée de nouveaux opponents
            * close : La meeting est fermé a l'entrée de nouveaux opponents mais n'as pas démarré
            * inProgress : La meeting est en cours de réalisation
            * finished : La meeting est terminé
            * error : La meeting à rencontré une erreur

        maps : ``objet`` - Caractéristiques des maps de la meeting

        conditions : ``objet`` - Conditions d'affrontement de cette meeting

        opponents : ``list`` - Liste des opponents s'afrontant dans cette meeting
    '''

    def __init__(self, id:str, slot:int, startTime:int, endTime:int, state:str = "open", maps:dict={}, conditions:dict={}):
        if startTime > endTime : raise ValueError("Le timestamp de départ n'est pas cohérent avec le timestamp de fin")
        super().__init__()
        self.id = id
        self.slot = slot
        self.startTime = startTime
        self.endTime = endTime
        self.state = state
        self.maps = maps
        self.conditions = conditions
        #self.__opponents = collections.OrderedDict()


    def set_score(self, opponentId:str, score):
        '''
        définir le score d'un opponent
        '''
        opponent = self.get_opponent(opponentId)
        #if not opponent: raise KeyError("Il n'y a pas d'opponent avec cette ID "+opponentId)
        opponent.score = score
        return opponent

    def add_score(self, opponentId:str, score):
        '''
        définir le score d'un opponent
        '''
        opponent = self.get_opponent(opponentId)
        #if not opponent: raise KeyError("Il n'y a pas d'opponent avec cette ID "+opponentId)
        opponent.score += score
        return opponent

    def setOpponentsHistory(self):
        for ii in self.getAll_opponents():
            history = []
            for iii in self.getAll_opponents():
                if self.get_opponent(ii).id != self.get_opponent(iii).id:
                    history.append(self.get_opponent(iii).id)

            self.get_opponent(ii).addHistoriqueOpponents(history)


class MeetingsManager():
    """"""
    def __init__(self):
        self.__meetings = collections.OrderedDict()

    def set_meeting(self, id: str, meeting):
        if self.__meetings.get(id):
            raise Exception("L'id '{}' existe déjà".format(id))
        self.__meetings.__setitem__(id, meeting)
        return self.get_meeting(id)

    def add_meeting(self, meetings):
        for i in meetings:
            meet = meetings[i]
            self.set_meeting(meet.id, meet)

    def get_meeting(self, id: str) -> Meeting:
        return self.__meetings.get(id)

    def getAll_meetings(self):
        return self.__meetings

    def remove_meeting(self, id: str) -> Meeting:
        return self.__meetings.pop(id)

    def reset_meetings(self):
        self.__meetings = collections.OrderedDict()

    def getAll_opponents(self):
        opponentsInput = []
        for i in self.getAll_meetings():
            meet = self.get_meeting(i)
            for ii in meet.getAll_opponents():
                opponentsInput.append(meet.get_opponent(ii))
        return opponentsInput
