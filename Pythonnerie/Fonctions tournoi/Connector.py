import collections

import sortedFiltre
from Meeting import Meeting, MeetingsManager


class Connector:
    """
    Class des connector reliant les meetings au sein d'un challenge
    """

    def __init__(self, id: str, name: str, challengeId: str, meetingsinput: list = [], meetingsOutput: list = [], filtres=["decroissant"]):
        self.id = id
        self.name = name
        self.challengeId = challengeId

        #self.meetingsInput = meetingsinput
        #self.meetingsOutput = meetingsOutput
        self.input = MeetingsManager()
        self.output = MeetingsManager()

        self.filtres = filtres
        self.classement = []


    def filter(self, opponents: list = None, triFunc: str = 'decroissant', attribut=lambda x: x.score):
        """
        Méthode permettant de creer le classement des "opponents" dans meetingsInput en se basant sur les "filtres"
        @return list ordonnée d'opponents 
        """
        if opponents == None : opponents = self.input.getAll_opponents() 
        # liste de filtre préfabriqués
        listeFiltres = {
            'decroissant': sortedFiltre.decroissant,
            'croissant': sortedFiltre.croissant,
            'noRematch': sortedFiltre.noRematch,
            'randomize':sortedFiltre.randomize
        }
        result, reste = listeFiltres[triFunc](opponents, attribut)

        return result, reste

    def checkMeetings(self, checkFunc=lambda meeting: meeting.state == 'finished') -> bool:
        """
        Vérifie que les "meetings" input sont bien terminés
        """
        for meet in self.input.getAll_meetings():
            if checkFunc(meet) == False:
                return False
        return True

    def fill(self):
        """
        Remplit les meetings suivantes ("meetingsOutput") avec le résultat de la fonction "filter"
        """
        opponentTofill = None
        for filtre in self.filtres:
            opponentTofill, reste = self.filter(self.input.getAll_opponents(),filtre)
        
        #TODO utiliser reste pour qeulqeu chose
        reste

        totalSlot = 0
        for i in self.output.getAll_meetings():
            meet = self.output.get_meeting(i)
            totalSlot += meet.slot

            for ii in range(meet.slot):
                meet.add_opponent(opponentTofill[(i*meet.slot+ii)])
        # Alerte de l'incoherence du nombre d'opposant vis a vis du nombre de slot
        if opponentTofill.__len__() != totalSlot:
            raise Exception("Il n ya pas un nombre coherent d opposant ({}) par rapport au nombre de meeting ({})".format(
                opponentTofill.__len__(), totalSlot))


class ConnectorsManager():
    """"""
    def __init__(self):
        self.__connectors = collections.OrderedDict()

    def set_connector(self, id: str, name: str, challengeId: str, meetingsinput: list = [], meetingsOutput: list = []):
        if self.get_connector(id):
            raise Exception("L'id '{}' existe déjà".format(id))
        self.__connectors.__setitem__(id, Connector(
            id, name, challengeId, meetingsinput, meetingsOutput, self.filters))
        return self.get_connector(id)

    def get_connector(self, id: str) -> Connector:
        return self.__connectors.get(id)

    def remove_connector(self, id: str) -> Connector:
        return self.__connectors.pop(id)

    def getAll_connector(self):
        return self.__connectors

    def reset_connectors(self):
        self.__connectors = collections.OrderedDict()

