import collections

class Opponent():
    '''
    Objet d'un utilisateur, team ou groupe au sein d'un challenge

    Parameters
    ----------
        objet : ``object`` - objet correspondant a l'opponent (joueur solo, team titulaire ou ephemere)

        challengeId : ``string`` - Id de l'challenge où se trouve l'opponent
    '''

    def __init__(self, id:str, name:str, challengeId:str, historiqueChallenge:list=[], historiqueOpponents:list = []):
        
        self.id = id
        self.name = name
        self.__historiqueChallenge = []
        self.__historiqueOpponents = []
        self.score = 0


        #TODO Pk pas  crer une factory ?

        if challengeId not in self.__historiqueChallenge:
            # Ajoute l'identifiant de l'challenge a l'historique si il n'est pas déjà présent
            self.__historiqueChallenge.append(challengeId)

    def getHistoriqueChallenge(self):
        return self.__historiqueChallenge

    def addHistoriqueChallenge(self, id:str):
        self.__historiqueChallenge.append(id)

    def removeHistoriqueChallenge(self, id:str):
        #TODO rechercher un id et le virer
        raise Exception("Cette fonction est en WIP")
        #self.__historiqueChallenge.append(id)


    def getHistoriqueOpponents(self):
        return self.__historiqueOpponents

    def addHistoriqueOpponents(self, id:str):
        self.__historiqueOpponents.append(id)

    def removeHistoriqueOpponents(self, id:str):
        raise Exception("Cette fonction est en WIP")

class OpponentsManager():
    """"""
    def __init__(self):
        self.__opponents = collections.OrderedDict()

    def set_opponent(self, id, opponent):
        if self.__opponents.get(id):
            raise Exception("L'id '{}' existe déjà".format(id))
        self.__opponents.__setitem__(id, opponent)
        return self.get_opponent(id)

    def get_opponent(self, id: str) -> Opponent:
        return self.__opponents.get(id)

    def getAll_opponents(self):
        return self.__opponents

    def remove_opponent(self, id: str) -> Opponent:
        return self.__opponents.pop(id)

    def reset_opponents(self):
        self.__opponents = collections.OrderedDict()

    def add_opponent(self, opponent):
        '''Ajouter un opponent au format Opponent'''
        return self.set_opponent(opponent.id, opponent)